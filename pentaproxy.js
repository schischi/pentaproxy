isGloabalModule = true;

var INFO =
["plugin" , { name: "proxy",
            version: "42",
            href: "https://bitbucket.org/schischi/pentaproxy",
            summary: "Proxy changer",
            xmlns: "dactyl" },
    ["author", { email: "adrien+dev@schischi.me" },
        "Adrien Schildknecht"]
    ["license", {},
        "BSD"],
    ["project", { name: "Pentadactyl", "min-version": "1.0"}],
    ["p", {},
        "Ths plugin allows you to switch the browser's proxy."],
    ["item", {},
        ["tags", {}, ":p :proxy"],
        ["spec", {}, ["ex", {}, ":proxy name"]],
        ["description", { short: "true"},
        ]
    ]
    ["item", {},
        ["tags", {}, ":np :noproxy"],
        ["spec", {}, ["ex", {}, ":noproxy"]],
        ["description", { short: "true"},
        ]
    ]
]

var defaultNoProxy = "127.0.0.1, localhost"
var proxy = {
    l:        [ "127.0.0.1",      "9999", "socks"       , "127.0.0.1, 10.1, 192.168.0.1/16"],
    sjtu:     [ "202.112.26.250", "8080", "http"        , "127.0.0.1" ],
    sjtu2:    [ "202.112.26.250", "8080", "http,ftp,ssl", "127.0.0.1" ],
    sjtu_all: [ "202.112.26.250", "8080", "all"         , "127.0.0.1" ]
    }

function set_proxy(host, port, type, exceptions){
    p = parseInt(port);

    if(type == "all") {
        prefs.set('network.proxy.share_proxy_settings', true);
        prefs.set('network.proxy.http', host);
        prefs.set('network.proxy.http_port', p);
    }
    else {
        t = type.split(',');
        prefs.set('network.proxy.share_proxy_settings', false);
        prefs.set('network.proxy.socks', "");
        prefs.set('network.proxy.http', "");
        prefs.set('network.proxy.ftp', "");
        prefs.set('network.proxy.ssl', "");
        for(var i = 0; i < t.length; i++) {
            prefs.set('network.proxy.' + t[i], host);
            prefs.set('network.proxy.' + t[i] + '_port', p);
        }
    }
    prefs.set('network.proxy.type', 1);
    prefs.set('network.proxy.no_proxies_on', exceptions);
}

function disable_proxy(){
    prefs.set('network.proxy.type', 0);
}

function set_proxy2(n){
    tmp = proxy[n];
    if (typeof(tmp) != 'undefined')
        set_proxy(tmp[0], tmp[1], tmp[2], tmp[3]);
    else
        dactyl.echo(n + ' not found');

}

function get_proxy(){
    var host = "";
    var type = "";
    var port = 0;
    if (prefs.get('network.proxy.type') != 0) {
        if(prefs.get('network.proxy.share_proxy_settings') == true) {
            host = prefs.get('network.proxy.http');
            port = prefs.get('network.proxy.http_port');
            type = "all ";
        }
        else {
            if (prefs.get('network.proxy.http') != "") {
                host = prefs.get('network.proxy.http');
                port = prefs.get('network.proxy.http_port');
                type += "http ";
            }
            if (prefs.get('network.proxy.ftp') != "") {
                host = prefs.get('network.proxy.ftp');
                port = prefs.get('network.proxy.ftp_port');
                type += "ftp ";
            }
            if (prefs.get('network.proxy.ssl') != "") {
                host = prefs.get('network.proxy.ssl');
                port = prefs.get('network.proxy.ssl_port');
                type += "ssl ";
            }
            if (prefs.get('network.proxy.socks') != "") {
                host = prefs.get('network.proxy.socks');
                port = prefs.get('network.proxy.socks_port');
                type += "socks ";
            }
        }
        exception = prefs.get('network.proxy.no_proxies_on');
        return "Proxy: " + host + ":" + port + " - " + type + " (" + exception + ")";
    }
    else {
        return "No proxy";
    }
}

group.commands.add(['proxy', 'p'],
    'Set the proxy',
    function (args){
        switch (args.length) {
            case 0:
                dactyl.echo(get_proxy());
                break;
            case 1:
                set_proxy2(args);
                dactyl.echo(get_proxy());
                break;
            case 2:
                [host, port] = args;
                set_proxy(host, port, "socks", "");
                dactyl.echo(get_proxy());
                break;
            case 3:
                [host, port, type] = args;
                set_proxy(host, port, type, "");
                dactyl.echo(get_proxy());
            case 4:
                [host, port, type, exceptions] = args;
                set_proxy(host, port, type, exceptions);
                dactyl.echo(get_proxy());
            default:
                dactyl.echo(':proxy host port [type default=socks] [exceptions]');
        }
    },
    {
    bang: true,
    completer: function(context, arg) {
        var completions = [];
        context.title = ['Name', 'Settings'];
        for (var k in proxy) {
            completions.push([k, proxy[k][0] + ":" +
                                 proxy[k][1] + " - " +
                                 proxy[k][2] + " (" +
                                 proxy[k][3] + ")"]);
        }
        context.completions = completions;}
    }, true);

group.commands.add(['noproxy', 'np'],
        'Disable the proxy',
        function (args){
            disable_proxy();
        },
        {}, true);
